package it.tiwiz.RootTest;

import android.app.Activity;

import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.Toast;
import com.stericson.RootTools.RootTools;
import com.stericson.RootTools.exceptions.RootDeniedException;
import com.stericson.RootTools.execution.Command;
import com.stericson.RootTools.execution.CommandCapture;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

public class RootActivity extends Activity implements OnClickListener
{
    /**
     * Called when the activity is first created.
     */

    Button btnRoot, btnBusyBox, btnComando;

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        btnRoot = (Button)findViewById(R.id.btnRoot);
        btnRoot.setOnClickListener(this);

        btnBusyBox = (Button)findViewById(R.id.btnBusybox);
        btnBusyBox.setOnClickListener(this);

        btnComando = (Button)findViewById(R.id.btnComando);
        btnComando.setOnClickListener(this);
    }

    @Override
    public void onClick(View view)
    {
        switch(view.getId()){
        case R.id.btnRoot:
            checkRoot();
            break;
        case R.id.btnBusybox:
            checkBusyBox();
            break;
        case R.id.btnComando:
            rootCommand();
            break;
        }
    }

    public void checkRoot(){

        if(RootTools.isRootAvailable()){
            if(RootTools.isAccessGiven())
                sendMessage("L'app ha ottenuto i permessi di root!");
            else RootTools.offerSuperUser(this);
        }else
            sendMessage("Attenzione, il dispositivo non sembra rootato!");

    }

    public void checkBusyBox(){

        if(RootTools.isBusyboxAvailable()) sendMessage("BusyBox disponibile!");
        else RootTools.offerBusyBox(this);
    }

    public void rootCommand(){

        CommandCapture command = new CommandCapture(0, "reboot");
        try
        {
            RootTools.getShell(true).add(command).waitForFinish();
        }
        catch (InterruptedException e)
        {
            sendMessage("ARGH! Sono stato interrotto");
        }
        catch (IOException e)
        {
            sendMessage("ARGH! Non so parlare, a quanto pare...");
        }
        catch (TimeoutException e)
        {
            sendMessage("Scusa il ritardo, ho trovato il semaforo P...");
        }
        catch (RootDeniedException e)
        {
            sendMessage("Voglio il ROOT! BURP!");
        }

    }

    public void sendMessage(String message){

        Toast.makeText(getApplicationContext(),message,Toast.LENGTH_SHORT).show();
    }
}
